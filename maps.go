package powerline

import (
	"cmp"
	"log/slog"
	"slices"
)

type levelValue struct {
	level       slog.Level
	value       string
	noColor     bool
	timePrefix  string
	timeSuffix  string
	levelPrefix string
	levelSuffix string
	attrPrefix  string
}

type levelMap struct {
	m map[slog.Level]*levelValue
	l []*levelValue
}

func newLevelMap(values []*levelValue) levelMap {
	l := make([]*levelValue, 0, len(values))
	m := make(map[slog.Level]*levelValue, len(values))

	for _, v := range values {
		l = append(l, v)
		m[v.level] = v
	}

	slices.SortFunc(l, func(a, b *levelValue) int {
		return cmp.Compare(b.level, a.level)
	})

	return levelMap{
		m: m,
		l: l,
	}
}

func newLevelMapFromString(src map[slog.Level]string) levelMap {
	values := make([]*levelValue, 0, len(src))

	for k, v := range src {
		values = append(values, &levelValue{level: k, value: v})
	}

	return newLevelMap(values)
}

func newLevelMapFromColors(colors map[slog.Level]ColorScheme) levelMap {
	values := make([]*levelValue, 0, len(colors))

	for k, s := range colors {
		c := NewColor(s.Level[1], s.Message[1])

		values = append(values, &levelValue{
			level:       k,
			noColor:     s.Time.isZero() && s.Level.isZero() && s.Message.isZero() && s.Attrs.isZero(),
			timePrefix:  s.Time.String(),
			timeSuffix:  " " + s.Level.String() + "\uE0B0",
			levelPrefix: s.Level.String() + " ",
			levelSuffix: " " + c.String() + "\uE0B0 " + s.Message.String(),
			attrPrefix:  s.Attrs.String() + " ",
		})
	}

	return newLevelMap(values)
}

func (m levelMap) get(level slog.Level) *levelValue {
	if v, ok := m.m[level]; ok {
		return v
	}

	for _, v := range m.l {
		if level >= v.level {
			return v
		}
	}

	return &levelValue{value: level.String()}
}
