package powerline

import (
	"fmt"
)

// Basic ANSI color definitions for 256-color terminals.
const (
	ColorDefault      = -1
	ColorBlack        = 0
	ColorRed          = 1
	ColorGreen        = 2
	ColorYellow       = 3
	ColorBlue         = 4
	ColorMagenta      = 5
	ColorCyan         = 6
	ColorLightGray    = 7
	ColorDarkGray     = 8
	ColorLightRed     = 9
	ColorLightGreen   = 10
	ColorLightYellow  = 11
	ColorLightBlue    = 12
	ColorLightMagenta = 13
	ColorLightCyan    = 14
	ColorWhite        = 15
)

// ColorScheme contains a color scheme for a log message.
// All colors are optional: the zero value will show all values in the default foreground/background color.
type ColorScheme struct {
	Time    Color
	Level   Color
	Message Color
	Attrs   Color
}

// Color contains an 8-bit ANSI color code pair (foreground and background).
// See https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit
type Color [2]int

// NewColor initializes a new terminal color.
func NewColor(fg, bg int) Color {
	return Color{fg, bg}
}

func (c Color) isZero() bool {
	var z Color

	return c == z
}

func (c Color) fg() string {
	if c[0] == ColorDefault || c.isZero() {
		return "39"
	}

	return fmt.Sprintf("38;5;%v", c[0])
}

func (c Color) bg() string {
	if c[1] == ColorDefault || c.isZero() {
		return "49"
	}

	return fmt.Sprintf("48;5;%v", c[1])
}

func (c Color) String() string {
	if c.isZero() {
		return ""
	}

	return "\033[" + c.fg() + ";" + c.bg() + "m"
}
