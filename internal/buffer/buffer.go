// Copyright 2022 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package buffer provides a pool-allocated byte buffer.
package buffer

import (
	"sync"
	"time"
)

// Buffer contains a buffer adapted from go/src/fmt/print.go.
type Buffer []byte

// Having an initial size gives a dramatic speedup.
var bufPool = sync.Pool{ //nolint:gochecknoglobals
	New: func() any {
		b := make([]byte, 0, 1024) //nolint:gomnd
		return (*Buffer)(&b)
	},
}

// New returns a new Buffer.
func New() *Buffer {
	return bufPool.Get().(*Buffer) //nolint:forcetypeassert
}

// Free returns a buffer to the pool.
func (b *Buffer) Free() {
	// To reduce peak allocation, return only smaller buffers to the pool.
	const maxBufferSize = 16 << 10

	if cap(*b) <= maxBufferSize {
		*b = (*b)[:0]
		bufPool.Put(b)
	}
}

// Write implements io.Writer.
func (b *Buffer) Write(p []byte) (int, error) {
	*b = append(*b, p...)

	return len(p), nil
}

// AppendString appends a string to the buffer.
func (b *Buffer) AppendString(s string) {
	*b = append(*b, s...)
}

// AppendBytes appends bytes to the buffer.
func (b *Buffer) AppendBytes(bytes []byte) {
	*b = append(*b, bytes...)
}

// AppendTime appends a time to the buffer.
func (b *Buffer) AppendTime(t time.Time, format string) {
	*b = t.AppendFormat(*b, format)
}
