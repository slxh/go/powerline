package buffer_test

import (
	"testing"
	"time"

	"gitlab.com/slxh/go/powerline/internal/buffer"
)

func Test(t *testing.T) {
	b := buffer.New()
	defer b.Free()

	b.AppendString("Hello ")
	_, _ = b.Write([]byte("World"))
	b.AppendBytes([]byte("! "))
	b.AppendTime(time.Unix(0, 0), time.DateOnly)

	if string(*b) != "Hello World! 1970-01-01" {
		t.Errorf("Incorrect buffer contents: %q", *b)
	}
}
