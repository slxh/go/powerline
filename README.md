# `powerline`: a slog.Handler that writes powerline output

Package `powerline` implements a zero-dependency [slog.Handler] that writes logs with [powerline] symbols.
The output format is inspired by [slog.TextHandler] and the magnificent [tint].

The icons and colors used can be customized by setting custom mappings using [HandlerOptions],
which is an extension of the [slog.HandlerOptions].  

## Usage

Basic usage:

```go
package main

import (
	"errors"
	"log/slog"
	"os"
	"time"

	"gitlab.com/slxh/go/powerline"
)

func main() {
	slog.SetDefault(slog.New(powerline.NewHandler(os.Stderr, &powerline.HandlerOptions{
		Level: slog.LevelDebug,
	})))

	slog.Debug("Starting to log things", "var", 1234)
	slog.Info("Logging a thing", "start", time.Now())
	slog.Warn("Something might break", "thing", "bork")
	slog.Error("Something broke", "err", errors.New("an error"))
}
```

![example output](.doc/example1.png)

And it can all be customized:

```go
package main

import (
	"errors"
	"log/slog"
	"os"
	"time"

	"gitlab.com/slxh/go/powerline"
)

func main() {
	slog.SetDefault(slog.New(powerline.NewHandler(os.Stderr, &powerline.HandlerOptions{
		Level:      slog.LevelDebug,
		TimeFormat: time.DateTime,
		Icons:      powerline.VerboseIcons,
		Colors:     powerline.NoColors,
	})))

	slog.Debug("Starting to log things", "var", 1234)
	slog.Info("Logging a thing", "start", time.Now())
	slog.Warn("Something might break", "thing", "bork")
	slog.Error("Something broke", "err", errors.New("an error"))
}
```

![example output](.doc/example2.png)

[HandlerOptions]: https://pkg.go.dev/gitlab.com/slxh/go/powerline#HandlerOptions
[slog]: https://pkg.go.dev/log/slog
[slog.Handler]: https://pkg.go.dev/log/slog#Handler
[slog.HandlerOptions]: https://pkg.go.dev/log/slog#HandlerOptions
[slog.TextHandler]: https://pkg.go.dev/log/slog#TextHandler
[powerline]: https://github.com/powerline/powerline
[tint]: https://github.com/lmittmann/tint
