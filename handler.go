// Package powerline implements a zero-dependency [slog.Handler] that writes logs with powerline symbols.
package powerline

import (
	"context"
	"fmt"
	"io"
	"log/slog"
	"sync"
	"time"

	"gitlab.com/slxh/go/powerline/internal/buffer"
)

const (
	ansiReset = "\033[0m"

	// DefaultAttrColor contains the default attribute foreground color (medium gray).
	DefaultAttrColor = 244
)

// DefaultIcons contains the default prefix mapping.
var DefaultIcons = map[slog.Level]string{ //nolint:gochecknoglobals
	slog.LevelDebug: "✚",
	slog.LevelInfo:  "✓",
	slog.LevelWarn:  "🗲",
	slog.LevelError: "✗",
}

// VerboseIcons contains an alternative set of levels with the default log level strings instead of icons.
var VerboseIcons = map[slog.Level]string{ //nolint:gochecknoglobals
	slog.LevelDebug: slog.LevelDebug.String(),
	slog.LevelInfo:  slog.LevelInfo.String(),
	slog.LevelWarn:  slog.LevelWarn.String(),
	slog.LevelError: slog.LevelError.String(),
}

// DefaultColors contains the default ANSI colors used for formatting messages.
var DefaultColors = map[slog.Level]ColorScheme{ //nolint:gochecknoglobals
	slog.LevelDebug: {
		Time:    NewColor(ColorDarkGray, ColorBlack),
		Level:   NewColor(ColorBlack, ColorLightGray),
		Message: NewColor(ColorLightGray, ColorDefault),
		Attrs:   NewColor(DefaultAttrColor, ColorDefault),
	},
	slog.LevelInfo: {
		Time:    NewColor(ColorDarkGray, ColorBlack),
		Level:   NewColor(ColorBlack, ColorLightGreen),
		Message: NewColor(ColorLightGreen, ColorDefault),
		Attrs:   NewColor(DefaultAttrColor, ColorDefault),
	},
	slog.LevelWarn: {
		Time:    NewColor(ColorDarkGray, ColorBlack),
		Level:   NewColor(ColorBlack, ColorLightYellow),
		Message: NewColor(ColorLightYellow, ColorDefault),
		Attrs:   NewColor(DefaultAttrColor, ColorDefault),
	},
	slog.LevelError: {
		Time:    NewColor(ColorDarkGray, ColorBlack),
		Level:   NewColor(ColorBlack, ColorLightRed),
		Message: NewColor(ColorLightRed, ColorDefault),
		Attrs:   NewColor(DefaultAttrColor, ColorDefault),
	},
}

// NoColors is a colors scheme with colors disabled.
var NoColors = map[slog.Level]ColorScheme{ //nolint:gochecknoglobals
	slog.LevelDebug: {},
	slog.LevelInfo:  {},
	slog.LevelWarn:  {},
	slog.LevelError: {},
}

// HandlerOptions is an extended version of [slog.HandlerOptions].
//
// The options can be modified to use different icons and colors.
// For example:
//
//	 &powerline.HandlerOptions{
//			Level:      slog.LevelDebug,
//			TimeFormat: time.DateTime,
//			Icons:      powerline.VerboseIcons,
//			Colors:     powerline.NoColors,
//		}
//
// Will result in output like:
//
//	2023-09-01 18:37:56  DEBUG  Starting to log things var=1234
//	2023-09-01 18:37:56  INFO  Logging a thing start=2023-09-01T18:37:56.908+02:00
//	2023-09-01 18:37:56  WARN  Something might break thing=bork
//	2023-09-01 18:37:56  ERROR  Something broke err="an error"
type HandlerOptions struct {
	// AddSource enables or disables source code location.
	AddSource bool

	// Level configures the minimum log level.
	Level slog.Leveler

	// ReplaceAttr is called to rewrite attributes before being logged.
	// See https://pkg.go.dev/log/slog#HandlerOptions for details.
	ReplaceAttr func(groups []string, a slog.Attr) slog.Attr

	// TimeFormat contains the time format used for formatting time.
	// No time is logged when left empty (the default).
	TimeFormat string

	// Colors contains a mapping log levels to ANSI color codes.
	// It defaults to [DefaultColors].
	Colors map[slog.Level]ColorScheme

	// Icons contains a mapping of colors to log levels.
	// It defaults to [DefaultIcons].
	Icons map[slog.Level]string
}

// Handler implements a [slog.Handler] with bright colors.
type Handler struct {
	opts    HandlerOptions
	handler slog.Handler
	colors  levelMap
	levels  levelMap
	mu      *sync.Mutex
	w       io.Writer
}

// NewHandler returns an initialized Handler.
func NewHandler(w io.Writer, opts *HandlerOptions) *Handler {
	if opts == nil {
		opts = new(HandlerOptions)
	}

	colors := opts.Colors
	if colors == nil {
		colors = DefaultColors
	}

	icons := opts.Icons
	if icons == nil {
		icons = DefaultIcons
	}

	h := &Handler{
		opts:   *opts,
		levels: newLevelMapFromString(icons),
		colors: newLevelMapFromColors(colors),
		mu:     new(sync.Mutex),
		w:      w,
	}
	h.handler = slog.NewTextHandler(w, &slog.HandlerOptions{
		AddSource:   opts.AddSource,
		Level:       opts.Level,
		ReplaceAttr: h.replaceAttr(opts.ReplaceAttr),
	})

	return h
}

func (h *Handler) replaceAttr(fn func([]string, slog.Attr) slog.Attr) func([]string, slog.Attr) slog.Attr {
	return func(groups []string, a slog.Attr) slog.Attr {
		switch a.Key {
		case slog.LevelKey, slog.TimeKey, slog.MessageKey:
			return slog.Attr{}
		default:
			if fn != nil {
				a = fn(groups, a)
			}

			return a
		}
	}
}

// Enabled returns true if records of the given log level are handled.
func (h *Handler) Enabled(_ context.Context, level slog.Level) bool {
	l := slog.LevelInfo

	if h.opts.Level != nil {
		l = h.opts.Level.Level()
	}

	return level >= l
}

// Handle handles the given [slog.Record].
func (h *Handler) Handle(ctx context.Context, record slog.Record) (err error) {
	colors := h.colors.get(record.Level)
	rep := h.opts.ReplaceAttr

	buf := buffer.New()
	defer buf.Free()

	if !colors.noColor {
		buf.AppendString(ansiReset)
	}

	if rep == nil {
		h.appendTime(buf, colors, record.Time)
	} else if a := rep(nil, slog.Time(slog.TimeKey, record.Time)); a.Key != "" {
		if a.Value.Kind() == slog.KindTime {
			h.appendTime(buf, colors, a.Value.Time())
		} else {
			h.appendTimeValue(buf, colors, a.Value.String())
		}
	}

	buf.AppendString(colors.levelPrefix)

	if rep == nil {
		buf.AppendString(h.levels.get(record.Level).value)
	} else if a := rep(nil, slog.Int(slog.LevelKey, int(record.Level))); a.Key != "" {
		if a.Value.Kind() == slog.KindInt64 {
			buf.AppendString(h.levels.get(slog.Level(a.Value.Int64())).value)
		} else {
			buf.AppendString(a.Value.String())
		}
	}

	buf.AppendString(colors.levelSuffix)
	buf.AppendString(record.Message)
	buf.AppendString(colors.attrPrefix)

	return h.write(ctx, buf, record, !colors.noColor)
}

func (h *Handler) write(ctx context.Context, buf *buffer.Buffer, record slog.Record, endColor bool) error {
	h.mu.Lock()
	defer h.mu.Unlock()

	if _, err := h.w.Write(*buf); err != nil {
		return fmt.Errorf("error handling record: %w", err)
	}

	if err := h.handler.Handle(ctx, record); err != nil {
		return fmt.Errorf("error handling record: %w", err)
	}

	if endColor {
		if _, err := h.w.Write([]byte(ansiReset)); err != nil {
			return fmt.Errorf("error handling record: %w", err)
		}
	}

	return nil
}

func (h *Handler) appendTimeValue(buf *buffer.Buffer, colors *levelValue, v string) {
	buf.AppendString(colors.timePrefix)
	buf.AppendString(v)
	buf.AppendString(colors.timeSuffix)
}

func (h *Handler) appendTime(buf *buffer.Buffer, colors *levelValue, ts time.Time) {
	if h.opts.TimeFormat != "" && !ts.IsZero() {
		buf.AppendString(colors.timePrefix)
		buf.AppendTime(ts, h.opts.TimeFormat)
		buf.AppendString(colors.timeSuffix)
	}
}

func (h *Handler) copy(handler slog.Handler) *Handler {
	return &Handler{
		opts:    h.opts,
		handler: handler,
		colors:  h.colors,
		levels:  h.levels,
		mu:      h.mu,
		w:       h.w,
	}
}

// WithAttrs returns a new Handler whose attributes are extended with the given attributes.
func (h *Handler) WithAttrs(attrs []slog.Attr) slog.Handler { //nolint:ireturn
	return h.copy(h.handler.WithAttrs(attrs))
}

// WithGroup returns a new Handler whose groups are extended with the given group name.
func (h *Handler) WithGroup(name string) slog.Handler { //nolint:ireturn
	return h.copy(h.handler.WithGroup(name))
}
