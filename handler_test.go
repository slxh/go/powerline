package powerline_test

import (
	"bytes"
	"context"
	"errors"
	"io"
	"log/slog"
	"os"
	"regexp"
	"testing"
	"time"

	"gitlab.com/slxh/go/powerline"
)

// Basic usage of powerline is simple.
// This looks better in a terminal than in the Go Playground.
func ExampleNewHandler() {
	slog.SetDefault(slog.New(powerline.NewHandler(os.Stdout, nil)))

	slog.Info("Hello world", "extra", 1234)

	// Output:
	// [0m[38;5;0;48;5;10m ✓ [38;5;10;49m [38;5;10;49mHello world[38;5;244;49m extra=1234
	// [0m
}

// It is possible to show the time by setting a time format.
// Additionally, replacing the time attribute will show any time of your choice.
func ExampleNewHandler_customTime() {
	log := slog.New(powerline.NewHandler(os.Stdout, &powerline.HandlerOptions{
		Level:      slog.LevelInfo,
		Colors:     powerline.NoColors,
		Icons:      powerline.VerboseIcons,
		TimeFormat: time.DateTime,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.TimeKey {
				return slog.Time(slog.TimeKey, time.Date(1970, 1, 1, 0, 0, 0, 0, time.Local))
			}

			return a
		},
	}))

	log.Info("Hello world", "extra", 1234)

	// Output:
	// 1970-01-01 00:00:00  INFO  Hello world extra=1234
}

// It is possible to override the time as any other value, which will always be shown.
func ExampleNewHandler_customTimeString() {
	log := slog.New(powerline.NewHandler(os.Stdout, &powerline.HandlerOptions{
		Level:  slog.LevelInfo,
		Colors: powerline.NoColors,
		Icons:  powerline.VerboseIcons,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.TimeKey {
				return slog.String(slog.TimeKey, "now")
			}

			return a
		},
	}))

	log.Info("Hello world", "extra", 1234)

	// Output:
	// now  INFO  Hello world extra=1234
}

// It is also possible to set the log level to a custom value by replacing it.
func ExampleNewHandler_customLevel1() {
	log := slog.New(powerline.NewHandler(os.Stdout, &powerline.HandlerOptions{
		Level:  slog.LevelInfo,
		Colors: powerline.NoColors,
		Icons:  powerline.VerboseIcons,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.LevelKey {
				return slog.String(slog.LevelKey, "CUSTOM")
			}

			return a
		},
	}))

	log.Info("Hello world")

	// Output:
	// CUSTOM  Hello world
}

// This also works for a log level, which is then interpreted with the default icons.
func ExampleNewHandler_customLevel2() {
	log := slog.New(powerline.NewHandler(os.Stdout, &powerline.HandlerOptions{
		Level:  slog.LevelInfo,
		Colors: powerline.NoColors,
		Icons:  powerline.DefaultIcons,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			// Replace the log level with the warning level.
			if a.Key == slog.LevelKey {
				return slog.Int(slog.LevelKey, int(slog.LevelWarn))
			}

			return a
		},
	}))

	log.Error("This was error")

	// Output:
	// 🗲  This was error
}

// Custom levels are logged as the closest (lower) level.
func ExampleNewHandler_undefinedLevel1() {
	log := slog.New(powerline.NewHandler(os.Stdout, &powerline.HandlerOptions{
		Level:  slog.LevelInfo,
		Colors: powerline.NoColors,
	}))

	log.Log(context.Background(), slog.LevelWarn+1, "This is a custom level")

	// Output:
	// 🗲  This is a custom level
}

// If a level is completely undefined, the result of [slog.Level.String] is shown.
func ExampleNewHandler_undefinedLevel2() {
	log := slog.New(powerline.NewHandler(os.Stdout, &powerline.HandlerOptions{
		Level:  slog.LevelInfo,
		Colors: powerline.NoColors,
		Icons:  map[slog.Level]string{},
	}))

	// Custom levels are logged as the closest (lower) level,
	// simple string conversion is used when no icon is defined.
	log.Log(context.Background(), slog.LevelError+1, "This is a custom level")

	// Output:
	// ERROR+1  This is a custom level
}

// It is possible to override the icons.
// Only the error level and higher are overridden,
// other (lower) levels default to the string value of the slog.Level.
func ExampleNewHandler_customIcons() {
	log := slog.New(powerline.NewHandler(os.Stdout, &powerline.HandlerOptions{
		Level:  slog.LevelInfo,
		Colors: powerline.NoColors,
		Icons: map[slog.Level]string{
			slog.LevelWarn: "a warning",
		},
	}))

	log.Error("Hello world")

	// Output:
	// a warning  Hello world
}

func TestHandler_Handle(t *testing.T) {
	buf := bytes.NewBuffer(nil)
	log := slog.New(powerline.NewHandler(buf, &powerline.HandlerOptions{
		Level:      slog.LevelInfo,
		Colors:     powerline.NoColors,
		TimeFormat: time.DateTime,
	}))
	dt := `\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}`
	re := regexp.MustCompile(`^` +
		dt + " \ue0b0 ✓ \ue0b0 TestMessage1 \n" +
		dt + " \ue0b0 ✓ \ue0b0 TestMessage2 \n")

	log.Info("TestMessage1")
	log.Info("TestMessage2")

	if !re.Match(buf.Bytes()) {
		t.Errorf("Incorrect string:\n%q\nRegex:\n%q", buf.String(), re.String())
	}
}

type errWriter struct {
	errAfter int
}

func (e *errWriter) Write(p []byte) (int, error) {
	if e.errAfter > 0 {
		e.errAfter--

		return len(p), nil
	}

	return 0, io.EOF
}

func TestHandler_Handle_errors(t *testing.T) {
	err1 := powerline.NewHandler(&errWriter{errAfter: 0}, nil).Handle(context.Background(), slog.Record{})
	err2 := powerline.NewHandler(&errWriter{errAfter: 1}, nil).Handle(context.Background(), slog.Record{})
	err3 := powerline.NewHandler(&errWriter{errAfter: 2}, nil).Handle(context.Background(), slog.Record{})

	if !errors.Is(err1, io.EOF) || !errors.Is(err2, io.EOF) || !errors.Is(err3, io.EOF) {
		t.Errorf("Expected errors: 1=%s 2=%s 3=%s", err1, err2, err3)
	}
}

func ExampleHandler_WithAttrs() {
	log := slog.New(powerline.NewHandler(os.Stdout, &powerline.HandlerOptions{
		Level:  slog.LevelInfo,
		Colors: powerline.NoColors,
	}))

	// Trust me, the powerline. look better in a terminal.
	log.With("extra", 12345).Info("Hello world")

	// Output:
	// ✓  Hello world extra=12345
}

func ExampleHandler_WithGroup() {
	log := slog.New(powerline.NewHandler(os.Stdout, &powerline.HandlerOptions{
		Level:  slog.LevelInfo,
		Colors: powerline.NoColors,
	}))

	// Trust me, the powerline. look better in a terminal.
	log.WithGroup("group").Info("Hello world", "extra", 123)

	// Output:
	// ✓  Hello world group.extra=123
}
