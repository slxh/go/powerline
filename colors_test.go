package powerline_test

import (
	"testing"

	"gitlab.com/slxh/go/powerline"
)

func TestColor_String(t *testing.T) {
	s := powerline.NewColor(powerline.ColorDefault, powerline.ColorDefault).String()

	if s != "\033[39;49m" {
		t.Errorf("Incorrect color: %q", s)
	}
}
