package powerline_test

import (
	"bytes"
	"context"
	"log/slog"
	"testing"
	"time"

	"gitlab.com/slxh/go/powerline"
)

func BenchmarkHandler_Handle(b *testing.B) {
	b.ReportAllocs()

	buf := new(bytes.Buffer)

	record := slog.NewRecord(time.Now(), slog.LevelInfo, "Test message", 0)
	record.Add("one", 1, "two", 2, "three", 3)

	h := powerline.NewHandler(buf, nil)

	for i := 0; i < b.N; i++ {
		_ = h.Handle(context.Background(), record)

		buf.Reset()
	}
}
